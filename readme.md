# Transfer200-SDK

Transfer200-SDK is a software development kit for the TCP-IP communication without application server.

### Version
0.1

### Tech

Transfer200-SDK uses a number of open source projects to work properly:

* [Java] - Main programming language
* [Android] - Android SDK!
* [Java Network Programming, 4th Edition] - Help to understanding TCP-IP at low-level
* [Gradle] - Building and scripting
* [GoF] - Help to tackle common problems on design

### Installation

TODO

```sh
$ ex. transfer200
```

```sh
$ etc1
$ etc2
```

### Todos

 - Write Tests
 - Add Code Comments

License
----

Apache License, Version 2.0