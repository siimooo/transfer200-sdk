package com.simosoft.transfer200sdk;

import com.simosoft.transfer200sdk.client.ConnectionMode;
import com.simosoft.transfer200sdk.client.SocketConnector;
import com.simosoft.transfer200sdk.common.StatusRouter;
import com.simosoft.transfer200sdk.common.reactor.InitiationDispatcher;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * TODO: write here description
 * User: Simo Ala-Kotila
 * Date: 03/01/16
 * Time: 18:48
 */
public class TestClient {
    public static void main(String[] args) throws IOException {
            InitiationDispatcher dispatcher = new InitiationDispatcher();
        //TODO: fix PendingCommand. some bug because multiple request act as one..
//            InetAddress address = InetAddress.getByName(new URL("http://httpbin.org/ip").getHost());
            InetAddress address = InetAddress.getByName(new URL("http://www.hs.fi").getHost());
            InetAddress address2 = InetAddress.getByName(new URL("http://www.google.fi").getHost());

//            InetAddress address = InetAddress.getByName("time.nist.gov");
            SocketConnector client = new SocketConnector(dispatcher);
            try {
                client.connect(new StatusRouter(), address, ConnectionMode.ASYNC, 80);
                client.connect(new StatusRouter(), address2, ConnectionMode.ASYNC, 80);
//                client.shutdown();
//                statusRouter.shutdown();
//                dispatcher.stop();
            } catch (IOException e) {
                e.printStackTrace();
            }
            dispatcher.handleEvents();
    }


    private static void artificialDelayOf(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
