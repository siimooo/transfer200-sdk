/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simosoft.transfer200sdk.common;

import com.simosoft.transfer200sdk.common.reactor.NioHandler;
import com.simosoft.transfer200sdk.common.reactor.ChangeRequest;
import com.simosoft.transfer200sdk.common.reactor.SocketHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * User: Simo Ala-Kotila
 * Date: 22.11.2015
 * Time: 14.18
 */
public class StatusRouter extends PeerRouter implements Runnable {

    private static final Logger LOG = LogManager.getLogger(StatusRouter.class);

    private final ExecutorService mExecutorService = Executors.newSingleThreadExecutor();

    @Override
    public void open() {
        if(mExecutorService.isShutdown() || mExecutorService.isTerminated()){
            LOG.debug("mExecutorService is shutdown or terminated");
            return;
        }
        LOG.debug("open");
        mExecutorService.execute(this);
    }

    @Override
    public void shutdown() {
        LOG.debug("shutdown");
        mExecutorService.shutdown();
        try {
            if (!mExecutorService.awaitTermination(10, TimeUnit.SECONDS)) {
                LOG.debug("shutdown still waiting after 10 second..");
            }
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void handleEvent(NioHandler<SocketHandler, SocketChannel> nioHandler, int eventType, SelectionKey selectionKey) {
        // Receive and route status data from/to Peers
        LOG.debug("handleEvent");
//        try {
//            if(selectionKey.interestOps() == SelectionKey.OP_WRITE){
//                LOG.debug("OP_WRITE");
//                Charset charset = Charset.forName("ISO-8859-1");
//                CharsetEncoder encoder = charset.newEncoder();
//                String request = "GET / HTTP/1.0\r\n\r\n";
//                nioHandler.getNioChannel().write(encoder.encode(CharBuffer.wrap(request)));
//                nioHandler.changeInterestedOps(this, SelectionKey.OP_READ, ChangeRequest.CHANGEOPS);
//            }else if(selectionKey.interestOps() ==SelectionKey.OP_READ){
//                LOG.debug("OP_READ");
//                ByteBuffer buffer = ByteBuffer.allocate(1024);
//                SocketChannel channel = getHandle().getNioChannel();
//                int n = channel.read(buffer);
//                if (n > 0) {
//                    buffer.flip();
//                    LOG.debug(new String(buffer.array(), 0, buffer.limit()));
//                    buffer.clear();
//                } else if (n == -1) {
//                    LOG.error("This shouldn't happen unless the server is misbehaving.");
//                }
//                nioHandler.changeInterestedOps(this, 0, ChangeRequest.UNREGISTER);
//            }
//        } catch (IOException e) {
//            LOG.error(e.getMessage());
//        }

    }





    @Override
    public void run() {
        LOG.debug("run "+ getHandle().getNioChannel());
        getHandle().changeInterestedOps(this, SelectionKey.OP_WRITE ,ChangeRequest.CHANGEOPS);
    }
}