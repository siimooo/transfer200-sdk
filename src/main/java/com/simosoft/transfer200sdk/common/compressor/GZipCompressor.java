/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.compressor;

import java.io.*;
import java.util.zip.GZIPOutputStream;

/**
 *
 * User: Simo Ala-Kotila
 * Date: 29.8.2015
 * Time: 17.09
 */
public class GZipCompressor extends Compressor{

    public GZipCompressor(File file) {
        super(file);
    }

    @Override
    public void accept(CompressorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void run() {
        super.run();
        File output = new File(getFile().getParent(), getFile().getName() + getPrefix());
        // Don't overwrite an existing file
        if(output.exists()){
            return;
        }

        //with try-with-resources block
        try(
                InputStream is = new BufferedInputStream(new FileInputStream(getFile()));
                OutputStream os = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(output.getName())))
        ){
            int b;
            while((b = is.read()) != -1){
                os.write(b);
            }

            os.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public String getPrefix() {
        return ".gz";
    }

}
