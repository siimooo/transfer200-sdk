/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.compressor;

import java.io.File;

/**
 *
 * User: Simo Ala-Kotila
 * Date: 30.8.2015
 * Time: 09.12
 */
public abstract class Compressor implements Runnable{

    private final File mFile;

    public Compressor(File file){
        mFile = file;
    }

    public abstract void accept(CompressorVisitor visitor);

    public abstract String getPrefix();

    @Override
    public void run() {
        // don't compress an already compressed file
        if(isCompressed()){
            return;
        }
        //TODO: java.io.FileNotFoundException: /home/simo/Downloads/c (Is a directory)
    }

    public boolean isCompressed(){
        return mFile.getName().endsWith(getPrefix());
    }

    public File getFile(){
        return mFile;
    }

}
