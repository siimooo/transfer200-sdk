/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.compressor;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * User: Simo Ala-Kotila
 * Date: 30.8.2015
 * Time: 17.18
 */
public class ZipCompressor extends Compressor {

    public ZipCompressor(File file) {
        super(file);
    }

    @Override
    public void run() {
        super.run();
        File output = new File(getFile().getParent(), getFile().getName() + getPrefix());
        // Don't overwrite an existing file
        if(output.exists()){
            return;
        }
        byte[] buffer = new byte[1024];
        // with try-with-resources block
        try(
                InputStream is = new BufferedInputStream(new FileInputStream(getFile()));
                ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(output)))
        ){
            zos.setMethod(ZipOutputStream.DEFLATED);
            zos.setLevel(5);
            zos.putNextEntry(new ZipEntry(getFile().getName()));
            int b;
            while((b = is.read(buffer)) != -1){
                zos.write(buffer, 0, b);
            }
            zos.closeEntry();
            zos.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void accept(CompressorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getPrefix() {
        return ".zip";
    }
}
