package com.simosoft.transfer200sdk.common.reactor;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * TODO: write here description
 * User: Simo Ala-Kotila
 * Date: 09/01/16
 * Time: 15:16
 *
 * @param <PEER> IPC mechanism that establishes connections actively
 * @param <HANDLE> Define OS's concrete mechanism handling NIO related operation
 */
public abstract class NioHandler<HANDLE extends NioHandler<HANDLE, PEER>, PEER>{

    private final PEER mChannel;

    private ConcurrentLinkedDeque<ChangeRequest> mChangeRequests;

    public NioHandler(PEER channel) {
        mChannel = channel;

    }

    public void changeInterestedOps(EventHandler<?, ?> eventHandler, int ops, int task){
        if(mChangeRequests == null){
            throw new RuntimeException("ChangeRequests list is null");
        }
        ChangeRequest request = new ChangeRequest(eventHandler, ops, task);
        mChangeRequests.add(request);
    }

//    public void close(NioHandler abstractNioHandler, SelectionKey selectionKey){
//        if(mChangeRequests == null){
//            throw new RuntimeException("ChangeRequests list is null");
//        }
//        ChangeRequest request = new ChangeRequest(abstractNioHandler, 0, ChangeRequest.UNREGISTER);
//        mChangeRequests.add(request);
//        // Finally, wake up our selecting thread so it can make the required changes
//        selectionKey.selector().wakeup();
//    }


    public abstract void doUnregister(Selector selector);

    public abstract void doRegister(Selector selector, ChangeRequest changeRequest) throws ClosedChannelException;

    public abstract void doChangeOps(Selector selector, ChangeRequest changeRequest);

    public abstract void read();

    public abstract void write();

    public abstract void doWrite(EventHandler<HANDLE, PEER> eventHandler, SelectionKey selectionKey);

    public abstract void doRead(EventHandler<HANDLE, PEER> eventHandler, SelectionKey selectionKey);

    public abstract void doConnect(EventHandler<HANDLE, PEER> eventHandler, SelectionKey selectionKey);


    public PEER getNioChannel(){
        return mChannel;
    }

    public void setChangeRequests(ConcurrentLinkedDeque<ChangeRequest> changeRequests){
        mChangeRequests = changeRequests;
    }
}