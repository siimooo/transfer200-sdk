package com.simosoft.transfer200sdk.common.reactor;

import java.nio.channels.SelectionKey;

/**
 * TODO: write here description
 * User: Simo Ala-Kotila
 * Date: 30/01/16
 * Time: 17:11
 */
public class ChangeRequest {

    /**
     * Register the
     */
    public static final int REGISTER = -1;

    /**
     * Unregister the
     */
    public static final int UNREGISTER = -2;

    /**
     * Change the ops
     */
    public static final int CHANGEOPS = 2;

    private final EventHandler<?, ?> mEventHandler;

    private final int mOps;

    private final int mTask;

    public ChangeRequest(EventHandler<?, ?> eventHandler, int ops, int task){
        mEventHandler = eventHandler;
        mOps = ops;
        mTask = task;
    }

    public EventHandler<?, ?> getEventHandler() {
        return mEventHandler;
    }

    public int getOps() {
        return mOps;
    }


    public int getTask() {
        return mTask;
    }
}
