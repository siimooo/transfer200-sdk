/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.reactor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

/**
 * TODO: write here description
 * User: Simo Ala-Kotila
 * Date: 10/01/16
 * Time: 12:04
 */
public class Dummy extends NioHandler {

    private static final Logger LOG = LogManager.getLogger(Dummy.class);

    public Dummy(SocketChannel channel) {
        super(channel);
    }

    @Override
    public void doUnregister(Selector selector) {

    }

    @Override
    public void doRegister(Selector selector, ChangeRequest changeRequest) throws ClosedChannelException {

    }

    @Override
    public void doChangeOps(Selector selector, ChangeRequest changeRequest) {

    }

    @Override
    public void read() {

    }

    @Override
    public void write() {

    }

    @Override
    public void doWrite(EventHandler eventHandler, SelectionKey selectionKey) {

    }

    @Override
    public void doRead(EventHandler eventHandler, SelectionKey selectionKey) {

    }


    @Override
    public void doConnect(EventHandler eventHandler, SelectionKey selectionKey) {

    }


}
