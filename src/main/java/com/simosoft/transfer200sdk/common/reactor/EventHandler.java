/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.reactor;

import java.io.IOException;
import java.nio.channels.SelectionKey;

/**
 * User: Simo Ala-Kotila
 * Date: 14.11.2015
 * Time: 20.52
 *
 * @param <PEER> IPC mechanism that establishes connections actively
 * @param <HANDLE> Define OS's concrete mechanism handling NIO related operation
 */
public interface EventHandler<HANDLE extends NioHandler<HANDLE, PEER>, PEER> {

    /**
     *
     * @param nioHandler
     * @param eventType
     * @param selectionKey
     */
    void handleEvent(NioHandler<HANDLE, PEER> nioHandler, int eventType, SelectionKey selectionKey);

    NioHandler<HANDLE, PEER> getHandle();
}