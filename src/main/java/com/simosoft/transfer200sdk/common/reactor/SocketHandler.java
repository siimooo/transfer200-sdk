/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.reactor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

/**
 * TODO: write here description
 * User: Simo Ala-Kotila
 * Date: 16/01/16
 * Time: 12:59
 */
public class SocketHandler extends NioHandler<SocketHandler, SocketChannel> {

    private static final Logger LOG = LogManager.getLogger(SocketHandler.class);

    public SocketHandler(SocketChannel channel) {
        super(channel);
    }

    @Override
    public void doUnregister(Selector selector) {
        LOG.debug("doUnregister");
        getNioChannel().keyFor(selector).cancel();
    }

    @Override
    public void doRegister(Selector selector, ChangeRequest changeRequest) throws ClosedChannelException {
        LOG.debug("doRegister");
        SocketChannel channel = getNioChannel();
        SelectionKey selectionKey = channel.register(selector, changeRequest.getOps());
        selectionKey.attach(changeRequest.getEventHandler());
    }

    @Override
    public void doChangeOps(Selector selector, ChangeRequest changeRequest) {
        LOG.debug("doChangeOps");
        SocketChannel channel = getNioChannel();
        SelectionKey selectionKey = channel.keyFor(selector);
        selectionKey.interestOps(changeRequest.getOps())
                    .attach(changeRequest.getEventHandler());
    }

    @Override
    public void read() {
        LOG.debug("read");
    }

    @Override
    public void write() {
        LOG.debug("write");

    }

    @Override
    public void doWrite(EventHandler<SocketHandler, SocketChannel> eventHandler, SelectionKey selectionKey) {
        LOG.debug("doWrite");
        eventHandler.handleEvent(this, getNioChannel().validOps(), selectionKey);
    }

    @Override
    public void doRead(EventHandler<SocketHandler, SocketChannel> eventHandler, SelectionKey selectionKey) {
        LOG.debug("doRead");
        eventHandler.handleEvent(this, getNioChannel().validOps(), selectionKey);
    }

    @Override
    public void doConnect(EventHandler<SocketHandler, SocketChannel> eventHandler, SelectionKey selectionKey) {
        LOG.debug("doConnect");
        eventHandler.handleEvent(this, getNioChannel().validOps(), selectionKey);
    }

}
