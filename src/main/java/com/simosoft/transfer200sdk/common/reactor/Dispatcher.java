/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.reactor;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;

/**
 * User: Simo Ala-Kotila
 * Date: 14.11.2015
 * Time: 20.48
 */
public interface Dispatcher {

    /**
     * Register an EventHandler of a particular
     *
     * @param eventHandler
     * @param eventType
     */
    void registerHandler(EventHandler<?, ?> eventHandler, int eventType);

    /**
     * Remove an EventHandler of a particular EventType
     *
     * @param eventHandler
     */
    void removeHandler(EventHandler<?, ?> eventHandler, SelectionKey selectionKey) throws IOException;

    /**
     * Entry point into the reactive event loop
     */
    void handleEvents();

    void stop();
}
