/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common.reactor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * When an application registers a Concrete Event
 * Handler with the Initiation Dispatcher the
 * application indicates the type of event(s) this Event
 * Handler wants the Initiation Dispatcher to
 * notify it aboutwhen the event(s) occur on the associated
 * AbstractHandle.
 *
 * The Initiation Dispatcher requests each
 * Event Handler to pass back its internal AbstractHandle.
 * This AbstractHandle identifies the Event Handler to the OS.
 *
 * After all Event Handlers are registered, an application
 * calls handle events to start the Initiation
 * Dispatcher’s event loop. At this point, the
 * Initiation Dispatcher combines the AbstractHandle
 * from each registered Event Handler and uses the
 * Synchronous Event Demultiplexer to wait
 * for events to occur on these Handles. For instance,
 * the TCP protocol layer uses the select synchronous
 * event demultiplexing operation to wait for
 * client logging record events to arrive on connected
 * socket Handles.
 *
 * The Initiation Dispatcher triggers Event
 * Handler hook method in response to events on
 * the ready Handles. When events occur, the
 * Initiation Dispatcher uses the Handles activated
 * by the event sources as “keys” to locate and
 * dispatch the appropriate Event Handler’s hook
 * method.
 *
 * The Initiation Dispatcher calls back to
 * the handle event hook method of the Event
 * Handler to perform application-specific functionality
 * in response to an event. The type of event that occurred
 * can be passed as a parameter to the method and used
 * internally by this method to perform additional servicespecific
 * demultiplexing and dispatching
 *
 * User: Simo Ala-Kotila
 * Date: 01/01/16
 * Time: 17.24
 */
public class InitiationDispatcher implements Dispatcher, Runnable {

    private final ConcurrentLinkedDeque<ChangeRequest> mChangeRequests = new ConcurrentLinkedDeque<>();

    private final ExecutorService reactorMain = Executors.newSingleThreadExecutor();

    private static final Logger LOG = LogManager.getLogger(InitiationDispatcher.class);

    private final Selector mSelector;

    public InitiationDispatcher() throws IOException {
        this.mSelector = Selector.open();
    }

    @Override
    public void registerHandler(EventHandler<?, ?> eventHandler, int eventType) {
        LOG.debug("registerHandler");
        NioHandler nioHandler = eventHandler.getHandle();

        //TODO: this why connection is bugged. Same Handler is added twice to mChangeRequests ?!?!
        //TODO: still Connection has in pending different handlers
        nioHandler.setChangeRequests(mChangeRequests);
        nioHandler.changeInterestedOps(eventHandler, eventType, ChangeRequest.REGISTER);
    }

    @Override
    public void removeHandler(EventHandler<?, ?> eventHandler, SelectionKey selectionKey) throws IOException {
        LOG.debug("removeHandler");
        eventHandler.getHandle().changeInterestedOps(eventHandler, 0, ChangeRequest.UNREGISTER);
    }

    @Override
    public void handleEvents() {
        reactorMain.execute(this);
    }

    @Override
    public void stop() {
        LOG.debug("stop");
        reactorMain.shutdownNow();
        try {
            mSelector.wakeup();
            reactorMain.awaitTermination(4, TimeUnit.SECONDS);
            mSelector.close();
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    protected void eventLoop() throws IOException {
        LOG.debug("Selector thread cycle begin...");
        while (true){

            //shutdown request
            if (Thread.interrupted()) {
                break;
            }

            // Process any pending changes
            Iterator<ChangeRequest> changeRequestIterator = mChangeRequests.iterator();
            while (changeRequestIterator.hasNext()){
                ChangeRequest changeRequest = changeRequestIterator.next();
                int task = changeRequest.getTask();
                switch (task){
                    case ChangeRequest.UNREGISTER:
                        LOG.debug("UNREGISTER");
                        changeRequest.getEventHandler().getHandle().doUnregister(mSelector);
                        changeRequestIterator.remove();
                        break;
                    case ChangeRequest.CHANGEOPS:
                        LOG.debug("CHANGEOPS");
                        changeRequest.getEventHandler().getHandle().doChangeOps(mSelector, changeRequest);
                        changeRequestIterator.remove();
                        break;
                    case ChangeRequest.REGISTER:
                        LOG.debug("REGISTER");
                        changeRequest.getEventHandler().getHandle().doRegister(mSelector, changeRequest);
                        changeRequestIterator.remove();
                        break;
                    default:
                        throw new IllegalArgumentException("not found task for " + task);
                }
                // Finally, wake up our selecting thread so it can make the required changes
                mSelector.wakeup();
            }

            //selector loop
            int readyChannels = mSelector.select();
            if(readyChannels == 0){
                continue;
            }
            Set<SelectionKey> selectedKeys = mSelector.selectedKeys();
            Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
            while(keyIterator.hasNext()) {
                SelectionKey key = keyIterator.next();

                if (!key.isValid()) {
                    LOG.debug("key not valid. remove it. KEY="+ key +", THREAD_ID="+Thread.currentThread().getId());
                    keyIterator.remove();
                    continue;
                }
                if(key.isAcceptable()) {
                    LOG.debug("a connection was accepted by a ServerSocketChannel. THREAD_ID="+Thread.currentThread().getId());
                } else if (key.isConnectable()) {
                    LOG.debug("a connection was established with a remote server. THREAD_ID="+Thread.currentThread().getId());
                    EventHandler eventHandler = (EventHandler) key.attachment();
                    eventHandler.getHandle().doConnect(eventHandler, key);
                } else if (key.isReadable()) {
                    LOG.debug("a channel is ready for reading. THREAD_ID="+Thread.currentThread().getId());
                    EventHandler eventHandler = (EventHandler) key.attachment();
                    eventHandler.getHandle().doRead(eventHandler, key);
                } else if (key.isWritable()) {
                    LOG.debug("a channel is ready for writing. THREAD_ID="+Thread.currentThread().getId());
                    EventHandler eventHandler = (EventHandler) key.attachment();
                    eventHandler.getHandle().doWrite(eventHandler, key);
                }
                keyIterator.remove();
            }
        }
    }

    @Override
    public void run() {
        try {
            eventLoop();
        } catch (IOException e) {
            LOG.error(e.getMessage());
            stop();
        }
    }
}