/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * User: Simo Ala-Kotila
 * Date: 7.11.2015
 * Time: 17.20
 */
public class DefaultSocket<T extends DefaultSocket.DefaultSocketBuilder<T>> {

    private static final Logger LOG = LogManager.getLogger(DefaultSocket.class);

    private final String mHostname;

    private final int mPort;

    private final InetAddress mInetAddress;

    public String getHostname(){
        return mHostname;
    }

    /**
     * Return port
     *
     * @return port
     */
    public int getPort(){
        return mPort;
    }

    /**
     * Return inetAddress
     *
     * @return InetAddress
     */
    public InetAddress getInetAddress(){
        return mInetAddress;
    }

    protected DefaultSocket(DefaultSocketBuilder<T> builder){
        mHostname = builder.mHostname;
        mPort = builder.mPort;
        mInetAddress = builder.mInetAddress;
    }

    public static class DefaultSocketBuilder<T extends DefaultSocketBuilder<T>>{

        private String mHostname;

        private int mPort = 8080;

        private InetAddress mInetAddress;

        public T setHostname(String hostname){
            mHostname = hostname;
            return (T) this;
        }

        /**
         * Set port value for connection. Default values is 8080
         *
         * @param port
         */
        public T setPort(int port){
            mPort = port;
            return (T) this;
        }

        public boolean setInetAddress(){
            try {
                if(mHostname == null){
                    mInetAddress = InetAddress.getLocalHost();
                    mHostname = mInetAddress.getHostName();
                }
                mInetAddress = InetAddress.getByName(mHostname);
                return false;
            }catch (UnknownHostException e) {
                LOG.error(e.getMessage(), e);
                return true;
            }
        }


        public DefaultSocket<T> build() {
            boolean isFailed = setInetAddress();
            if(isFailed){
                return null;
            }
            return new DefaultSocket<>(this);
        }
    }

}
