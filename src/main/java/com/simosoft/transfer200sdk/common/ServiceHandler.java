/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common;

import com.simosoft.transfer200sdk.common.reactor.NioHandler;
import com.simosoft.transfer200sdk.common.reactor.EventHandler;

import java.net.InetAddress;
import java.nio.channels.SelectionKey;

/**
 * User: Simo Ala-Kotila
 * Date: 14.11.2015
 * Time: 20.29
 *
 * @param <PEER> IPC mechanism that establishes connections actively
 * @param <SH> Define concrete class of ServiceHandler
 * @param <HANDLE> Define OS's concrete mechanism handling NIO related operation
 */
public abstract class ServiceHandler<PEER, SH extends ServiceHandler, HANDLE extends NioHandler<HANDLE, PEER>> implements EventHandler<HANDLE, PEER>{

    /**
     * Template placeholder for a concrete IPC mechanism wrapper facade,
     * which encapsulate a data-mode transport endpoint and
     */
    protected PEER mIPCStream;

    protected NioHandler<HANDLE, PEER> mHandle;

    public abstract void open();

    public PEER peer(){
        return mIPCStream;
    }

    public void setHandle(NioHandler<HANDLE, PEER> nioHandler){
        mHandle = nioHandler;
    }

    @Override
    public NioHandler<HANDLE, PEER> getHandle() {
        return mHandle;
    }

    public PEER getIPCStream() {
        return mIPCStream;
    }
}
