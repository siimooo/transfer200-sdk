/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.server.monitor;


import com.simosoft.transfer200sdk.server.monitor.observer.SocketObservable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * User: Simo Ala-Kotila
 * Date: 12.9.2015
 * Time: 20.51
 */
public abstract class Monitor extends SocketObservable {

    private static final Logger LOG = LogManager.getLogger(Monitor.class);

    /**
     * Shutdown monitor flag
     */
    private AtomicBoolean mShutdown = new AtomicBoolean(false);

    public void start(){
        LOG.debug("start");
        onStart();
        while (!mShutdown.get()) {
            onProcess();
        }
        onStop();
    }

    protected void onStart() {
        LOG.debug("onStart");
    }

    protected void onProcess(){
        LOG.debug("onProcess");
    }

    /**
     * Shutdown this monitor
     */
    public void shutdown(){
        LOG.debug("shutdown");
        mShutdown.getAndSet(true);
        notifyShutdown();
    }

    protected void onStop(){
        LOG.debug("onStop");
    }

}
