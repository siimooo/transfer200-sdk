/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.server.monitor.observer;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Simo Ala-Kotila
 * Date: 17.10.2015
 * Time: 15.30
 */
public class SocketObservable {

    private List<SocketObserver> mObservers;

    public SocketObservable(){
        mObservers = new ArrayList<>();
    }

    public void addObserver(SocketObserver observer){
        mObservers.add(observer);
    }

    public void notifyNewConnection(Socket socket){
        for(SocketObserver observer : mObservers){
            observer.onNewConnection(socket);
        }
    }

    public void notifyShutdown(){
        for(SocketObserver observer : mObservers){
            observer.onShutdown();
        }
    }


}
