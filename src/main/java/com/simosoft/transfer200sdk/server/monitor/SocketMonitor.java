/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.server.monitor;

import com.simosoft.transfer200sdk.common.exception.ServerSocketException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 *
 *
 * User: Simo Ala-Kotila
 * Date: 17.10.2015
 * Time: 14.00
 */
public class SocketMonitor extends Monitor implements Runnable {

    private static final Logger LOG = LogManager.getLogger(SocketMonitor.class);

    /**
     *
     */
    private final SocketConfig mSocketConfig;

    /**
     *
     */
    private final ServerSocket mSocketServer;

    private int counter = 0;

    public SocketMonitor(){
        super();
        mSocketConfig = getDefaultSocketConfig();
        mSocketServer = getSocketServer();
    }

    public SocketMonitor(SocketConfig socketConfig){
        super();
        mSocketConfig = socketConfig;
        mSocketServer = getSocketServer();
    }

    private final SocketConfig getDefaultSocketConfig(){
        return new SocketConfig.SocketConfigBuilder<>().build();
    }

    private final ServerSocket getSocketServer(){
        try {
            return new ServerSocket(mSocketConfig.getPort(), mSocketConfig.getQueueSize(), mSocketConfig.getInetAddress());
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return null;
        }
    }

    @Override
    protected void onProcess() {
        super.onProcess();
        LOG.debug("Wait the Socket..");
        Socket socket = null;
        try{
            socket = mSocketServer.accept();
            log(socket);
            notifyNewConnection(socket);
            socket.close();
        }catch (IOException ex){
            LOG.error(ex.getMessage());
        }catch (RuntimeException ex){
            LOG.error(ex.getMessage(), ex);
        }finally{
            //TODO: http://stackoverflow.com/questions/516049/in-java-is-the-finally-block-guaranteed-to-be-called-in-the-main-method
            if(socket == null){
                return;
            }
            try {
                LOG.info("finally");
                socket.close();
            }catch (IOException e) {}
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mSocketServer == null){
            throw new ServerSocketException("ServerSocket is null");
        }
    }

    @Override
    public void run() {
        start();
    }


    @Override
    protected void onStop() {
        super.onStop();
        try {
            mSocketServer.close();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    private void log(Socket socket) {
        counter++;
        Date now = new Date();
        LOG.info(now + " " + socket.getRemoteSocketAddress() + " " + counter);
    }

}
