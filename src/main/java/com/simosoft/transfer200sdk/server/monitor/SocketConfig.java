/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.server.monitor;

import com.simosoft.transfer200sdk.common.DefaultSocket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * User: Simo Ala-Kotila
 * Date: 7.11.2015
 * Time: 16.55
 */
public class SocketConfig <T extends SocketConfig.SocketConfigBuilder<T>> extends DefaultSocket<T> {

    //303, 308, 309, 325, 329,332

    private static final Logger LOG = LogManager.getLogger(SocketConfig.class);

    private final int mQueueSize;

    public int getQueueSize(){
        return mQueueSize;
    }

    protected SocketConfig(SocketConfigBuilder<T> builder){
        super(builder);
        mQueueSize = builder.mQueueSize;
    }

    /**
     * Builder class for server socket.
     */
    public static class SocketConfigBuilder <T extends SocketConfig.SocketConfigBuilder<T>> extends DefaultSocket.DefaultSocketBuilder<T>{

        private int mQueueSize = 50;

        /**
         * If you try to expand the queue past the operating system’s maximum queue length, the
         * maximum queue length is used instead.
         *
         * @param queueSize
         * @return
         */
        public T setQueueSize(int queueSize) {
            mQueueSize = queueSize;
            return (T) this;
        }

        public SocketConfig build() {
            boolean isFailed = setInetAddress();
            if(isFailed){
                return null;
            }
            return new SocketConfig(this);
        }

    }

}
