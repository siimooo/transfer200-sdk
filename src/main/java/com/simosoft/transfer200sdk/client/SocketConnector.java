/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.client;

import com.simosoft.transfer200sdk.common.PeerRouter;
import com.simosoft.transfer200sdk.common.reactor.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * User: Simo Ala-Kotila
 * Date: 25.10.2015
 * Time: 16.49
 */
public class SocketConnector extends Connector<SocketConnector, SocketChannel, PeerRouter, SocketHandler>{

    private static final Logger LOG = LogManager.getLogger(SocketConnector.class);

    private NioHandler mNioHandler;

    public SocketConnector(Dispatcher dispatcher) throws IOException {
        super(dispatcher);
    }

    @Override
    public void handleEvent(NioHandler<SocketHandler, SocketChannel> nioHandler, int eventType, SelectionKey selectionKey) {
        LOG.debug("handleEvent");
        super.handleEvent(nioHandler, eventType, selectionKey);
    }

    @Override
    public NioHandler<SocketHandler, SocketChannel> getHandle() {
        return mNioHandler;
    }

    @Override
    public void shutdown() {
        LOG.debug("shutdown");
    }

    @Override
    public void connect(PeerRouter peerRouter, InetAddress inetAddress, ConnectionMode mode, int port) throws IOException {
        mPeer = SocketChannel.open();
        if(mode == ConnectionMode.ASYNC){
            mPeer.configureBlocking(false);
        }
        InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, port);
        mPeer.connect(inetSocketAddress);
        mNioHandler = new SocketHandler(mPeer);
        super.connect(peerRouter, inetAddress, mode, port);
    }

    @Override
    protected void complete(NioHandler<SocketHandler, SocketChannel> nioHandler, SelectionKey selectionKey) throws IOException {
        //call finish connection
        nioHandler.getNioChannel().finishConnect();
        super.complete(nioHandler, selectionKey);
    }
}