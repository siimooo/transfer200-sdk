/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.client;


import com.simosoft.transfer200sdk.common.ServiceHandler;
import com.simosoft.transfer200sdk.common.reactor.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.channels.SelectionKey;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User: Simo Ala-Kotila
 * Date: 14.11.2015
 * Time: 20.31
 *
 * @param <C> Define concrete class of Connector
 * @param <PEER> IPC mechanism that establishes connections actively
 * @param <SH> Define concrete class of ServiceHandler
 * @param <HANDLE> Define OS's concrete mechanism handling NIO related operation
 */
public abstract class Connector<C extends Connector<C, PEER, SH, HANDLE>, PEER, SH extends ServiceHandler<PEER, SH, HANDLE>, HANDLE extends NioHandler<HANDLE, PEER>> implements EventHandler<HANDLE, PEER> {

    private static final Logger LOG = LogManager.getLogger(Connector.class);

    private final ExecutorService mConnector = Executors.newSingleThreadExecutor();

    //TODO: check the NioHandler generic parameter
    protected Map<NioHandler<HANDLE, PEER>, SH> mHandleMap = new ConcurrentHashMap<>();

    protected final Dispatcher mDispatcher;

    protected PEER mPeer;

    public Connector(Dispatcher dispatcher){
        mDispatcher = dispatcher;
    }

    /**
     * Actively connecting and activate a service.
     *
     * @param sh concrete class of SH
     * @param inetAddress to connection address
     * @param mode sync or async connection mode
     *
     */
    public void connect(SH sh, InetAddress inetAddress, ConnectionMode mode, int port) throws IOException {
        LOG.debug("connect");
        connectServiceHandler(sh, inetAddress, mode, port);
    }


    /**
     * Defines the active connection strategy.
     *
     * @param sh
     * @param inetAddress
     * @param mode
     */
    protected void connectServiceHandler(SH sh, InetAddress inetAddress, ConnectionMode mode, int port) {
        LOG.debug("connectServiceHandler " + mode);
        //Delegate to concrete PEER_CONNECTOR to establish the connection.
        if(mode == ConnectionMode.ASYNC){
            // If the connection hasn’t completed and we are using non-blocking semantics then
            // register ourselves with the Reactor so that it will callback when the connection is complete.
            mDispatcher.registerHandler(this, SelectionKey.OP_CONNECT);
            // Store the SERVICE_HANDLER in the map of pending connections.
            mHandleMap.put(getHandle(), sh);
        }else{
            sh.setHandle(getHandle());
            activateServiceHandler(sh);
        }
    }

    /**
     * Defines the handler’s concurrency strategy.
     *
     * @param sh
     *
     */
    protected void activateServiceHandler(SH sh){
        LOG.debug("activateServiceHandler");
        sh.open();
    }

    /**
     * Activate a SERVICE_HANDLER whose non-blocking connection completed.
     */
    protected void complete(NioHandler<HANDLE, PEER> nioHandler, SelectionKey selectionKey) throws IOException {
        // Locate the SERVICE_HANDLER nioHandler to the HANDLE.
        SH sh = mHandleMap.get(nioHandler);

        // Transfer I/O handle to SERVICE_HANDLER
        sh.setHandle(nioHandler);

        // Remove handle from Initiation_Dispatcher.
        mHandleMap.remove(nioHandler);
        // Peer is complete, so activate handler.
        LOG.debug("connection is now completed, THREAD_ID=" + Thread.currentThread().getId() +", PENDING= " +mHandleMap.size());

//        activateServiceHandler(sh);
    }

    /**
     * Activate a SH whose non-blocking connection has completed successfully
     *
     * @param nioHandler
     * @param eventType
     * @param selectionKey
     *
     * @see ServiceHandler
     */
    @Override
    public void handleEvent(NioHandler<HANDLE, PEER> nioHandler, int eventType, SelectionKey selectionKey) {
        //find concrete ipc
        try{
            complete(nioHandler, selectionKey);
        }catch (IOException e){
            LOG.error(e.getMessage());
            nioHandler.changeInterestedOps(this, 0, ChangeRequest.UNREGISTER);
//            http://stackoverflow.com/questions/13347014/java-nio-connect-to-socket
        }
    }

    public abstract void shutdown();
}
