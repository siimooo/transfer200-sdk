/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.client;

import com.simosoft.transfer200sdk.common.DefaultSocket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * User: Simo Ala-Kotila
 * Date: 25.10.2015
 * Time: 17.13
 */
public class Address<T extends Address.RequestBuilder<T>> extends DefaultSocket<T>{

    public static final Logger LOG = LogManager.getLogger(Address.class);

    private final int mTimeout;

    public int getTimeout(){
        return mTimeout;
    }

    protected Address(RequestBuilder<T> builder){
        super(builder);
        mTimeout = builder.mTimeout;
    }

    public static class RequestBuilder<T extends Address.RequestBuilder<T>> extends DefaultSocket.DefaultSocketBuilder<T>{

        private int mTimeout = 15000;

        /**
         * Set timeout value for connection. Default values is 15000ms (15 seconds)
         *
         * @param timeout
         */
        public T setTimeout(int timeout){
            mTimeout = timeout;
            return (T) this;
        }

        public Address<T> build() {
            boolean isFailed = setInetAddress();
            if(isFailed){
                return null;
            }
            return new Address(this);
        }
    }

}