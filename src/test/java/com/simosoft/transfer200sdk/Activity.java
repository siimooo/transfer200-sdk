/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk;


import com.simosoft.transfer200sdk.client.ConnectionMode;
import com.simosoft.transfer200sdk.client.SocketConnector;
import com.simosoft.transfer200sdk.common.StatusRouter;
import com.simosoft.transfer200sdk.common.reactor.InitiationDispatcher;
import com.simosoft.transfer200sdk.server.monitor.observer.SocketObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.IOException;
import java.net.*;

/**
 * User: Simo Ala-Kotila
 * Date: 18.10.2015
 * Time: 11.59
 */
public class Activity implements SocketObserver {

    private static final Logger LOG = LogManager.getLogger(Activity.class);

    @Test
    public void onCreate() throws IOException {
//        SocketMonitor monitor = new SocketMonitor();
//        SocketManager manager = new SocketManager();
//        monitor.addObserver(this);
//        monitor.addObserver(manager);
//
//
//        Thread t = new Thread(monitor, "SocketMonitor");
//        t.start();

        InitiationDispatcher dispatcher = new InitiationDispatcher();
        StatusRouter statusRouter = new StatusRouter();

        SocketConnector client = new SocketConnector(dispatcher);
        try {

//            InetAddress address = InetAddress.getByName(new URL("http://www.iltasanomat.fi").getHost());
//            String ip = address.getHostAddress();
//            client.connect(statusRouter, InetAddress.getByName(ip), ConnectionMode.SYNC);

            client.connect(statusRouter, InetAddress.getLocalHost(), ConnectionMode.ASYNC, 8080);


//            client.connect(statusRouter, InetAddress.getLocalHost(), ConnectionMode.ASYNC);




            dispatcher.handleEvents();

//            client.shutdown();
//            dispatcher.stop();
//                        System.out.println(i);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNewConnection(Socket socket) {
        LOG.info("onNewConnection");
    }

    @Override
    public void onShutdown() {
        LOG.info("onShutdown");
    }
}
