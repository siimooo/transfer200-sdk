/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.client;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: Simo Ala-Kotila
 * Date: 7.11.2015
 * Time: 13.27
 */
public class RequestTest {

    private Address request;

    private static final String HOSTNAME = "190.168.15.20";
    private static final int PORT = 8080;

    @Before
    public void setUp(){
        request = new Address.RequestBuilder<>().build();
    }

    @Test
    public void testGetInetAddress() throws Exception {
        assertNotNull(request.getInetAddress());
    }

    @Test
    public void testGetTimeout() throws Exception {
        assertEquals(request.getTimeout(), 15000);
    }

    @Test
    public void testChangeTimeout(){
        Address timeout = new Address.RequestBuilder().setTimeout(1500).build();
        assertEquals(timeout.getTimeout(), 1500);
    }

}