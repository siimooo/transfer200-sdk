package com.simosoft.transfer200sdk.common.reactor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.channels.SelectionKey;

/**
 * Created by simoala-kotila on 01/01/16.
 */
public class ReactorTest implements EventHandler{

    private NioHandler handle;
    private InitiationDispatcher dispatcher;

    private static final Logger LOG = LogManager.getLogger(ReactorTest.class);


    @Before
    public void setUp() throws IOException {
        LOG.debug("setup reactor test");
        dispatcher = new InitiationDispatcher();
        handle = new Dummy(null);

//        dispatcher.registerHandler(new Dummy(null), SelectionKey.OP_ACCEPT);
    }

    @Test
    public void test1(){
        dispatcher.handleEvents();
    }



    @Override
    public void handleEvent(NioHandler abstractNioChannel, int selectionKey, SelectionKey key) {

    }

    @Override
    public NioHandler getHandle() {
        return handle;
    }
}
