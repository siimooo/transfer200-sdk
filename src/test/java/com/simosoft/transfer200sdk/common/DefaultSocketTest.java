/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.common;

import com.simosoft.transfer200sdk.client.Address;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * User: Simo Ala-Kotila
 * Date: 7.11.2015
 * Time: 18.03
 */
public class DefaultSocketTest {

    private DefaultSocket request;

    private static final String HOSTNAME = "190.168.15.20";
    private static final int PORT = 8080;

    @Before
    public void setUp(){
        request = new DefaultSocket.DefaultSocketBuilder<>().setHostname(HOSTNAME).build();
    }


    @Test
    public void testGetInetAddress() throws Exception {
        assertNotNull(request.getInetAddress());
    }


    @Test
    public void testGetHostname() throws Exception {
        assertEquals(request.getHostname(), HOSTNAME);
    }

    @Test
    public void testGetPort() throws Exception {
        assertEquals(request.getPort(), PORT);
    }

    @Test
    public void testFailRequest(){
        Address fail = new Address.RequestBuilder<>().setHostname("a").build();
        assertNull(fail);
    }

    @Test
    public void testNullHostname(){
        Address notnull = new Address.RequestBuilder<>().build();
        assertNotNull(notnull.getHostname());
    }

    @Test
    public void testChangePort(){
        Address port = new Address.RequestBuilder<>().setPort(50).build();
        assertEquals(port.getPort(), 50);
    }

}