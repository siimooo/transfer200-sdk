/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simosoft.transfer200sdk;

import com.simosoft.transfer200sdk.common.compressor.ZipCompressor;
import org.junit.Test;

import java.io.File;

/**
 *
 * User: Simo Ala-Kotila
 * Date: 29.8.2015
 * Time: 17.29
 */
public class CompressorsTest {

    @Test
    public void testCompressorsTest(){
        File file = new File("/home/simo/Downloads/2.jpg");
        ZipCompressor gzip = new ZipCompressor(file);
        Thread t = new Thread((gzip));
        t.start();
    }
}
