/*
 * Copyright 2015 Simo Ala-Kotila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simosoft.transfer200sdk.server.monitor;

import com.simosoft.transfer200sdk.server.monitor.observer.SocketObserver;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.Socket;

import static org.junit.Assert.assertTrue;

/**
 * User: Simo Ala-Kotila
 * Date: 24.10.2015
 * Time: 11.00
 */
public class SocketMonitorTest{

    private static SocketMonitor mSocketMonitor;


    @BeforeClass
    public static void setUp() throws Exception {
        SocketConfig socketConfig =  new SocketConfig.SocketConfigBuilder<>().setPort(8081).build();
        mSocketMonitor = new SocketMonitor(socketConfig);
    }

    @Test
    public void testShutdown() throws Exception {
        Thread t = new Thread(mSocketMonitor);
        t.start();
        long starttime = System.currentTimeMillis();
        Thread.sleep(1000);
        mSocketMonitor.shutdown();
        long endtime = System.currentTimeMillis();
        long total = endtime - starttime;
        assertTrue(total >= 1000 && total <= 1005);
    }

    @Test
    public void testOnStop() throws Exception {

    }

    @Test
    public void testAddObserver() throws Exception {
        mSocketMonitor.addObserver(new SocketObserver() {
            @Override
            public void onNewConnection(Socket socket) {
                boolean change = false;
                change = true;
                assertTrue(change);
            }

            @Override
            public void onShutdown() {
                boolean change = false;
                change = true;
                assertTrue(change);
            }
        });
    }

    @Test
    public void testOnNewConnection() throws Exception {
        mSocketMonitor.notifyNewConnection(null);
    }

    @Test
    public void testOnShutdown() throws Exception {
        mSocketMonitor.notifyShutdown();
    }

    @Test
    public void onNewConnection() {
        boolean change = false;
        change = true;
        assertTrue(change);
    }

    @Test
    public void onShutdown() {
        boolean change = false;
        change = true;
        assertTrue(change);
    }

    //TODO: write serversocketexception

}